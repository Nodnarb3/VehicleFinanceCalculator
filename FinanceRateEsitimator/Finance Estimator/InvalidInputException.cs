﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance_Estimator
{
    class InvalidInputException : Exception
    {
        String message;
        public InvalidInputException(String message)
        {
            this.message = message;
        }

        public override string ToString()
        {
            return message;
        }
    }
}
