﻿namespace Finance_Estimator
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings));
            this.SettingsGroup = new System.Windows.Forms.GroupBox();
            this.TermLengthInputLabel = new System.Windows.Forms.Label();
            this.TermLengthAsYears = new System.Windows.Forms.CheckBox();
            this.TermLengthAsMonths = new System.Windows.Forms.CheckBox();
            this.GSTLabel = new System.Windows.Forms.Label();
            this.GST = new System.Windows.Forms.NumericUpDown();
            this.Taxes = new System.Windows.Forms.Label();
            this.IncludeGST = new System.Windows.Forms.CheckBox();
            this.CompoundingTermMonthsLabel = new System.Windows.Forms.Label();
            this.CompoundingTerm = new System.Windows.Forms.NumericUpDown();
            this.CompoundingTermLabel = new System.Windows.Forms.Label();
            this.InterestRateLabel = new System.Windows.Forms.Label();
            this.InterestRateAsDecimal = new System.Windows.Forms.CheckBox();
            this.InterestRateAsPercent = new System.Windows.Forms.CheckBox();
            this.InfoGroup = new System.Windows.Forms.GroupBox();
            this.SettingInfo = new System.Windows.Forms.RichTextBox();
            this.ShowTotal = new System.Windows.Forms.CheckBox();
            this.OutputOptionsLabel = new System.Windows.Forms.Label();
            this.SettingsGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GST)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompoundingTerm)).BeginInit();
            this.InfoGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // SettingsGroup
            // 
            this.SettingsGroup.Controls.Add(this.OutputOptionsLabel);
            this.SettingsGroup.Controls.Add(this.ShowTotal);
            this.SettingsGroup.Controls.Add(this.TermLengthInputLabel);
            this.SettingsGroup.Controls.Add(this.TermLengthAsYears);
            this.SettingsGroup.Controls.Add(this.TermLengthAsMonths);
            this.SettingsGroup.Controls.Add(this.GSTLabel);
            this.SettingsGroup.Controls.Add(this.GST);
            this.SettingsGroup.Controls.Add(this.Taxes);
            this.SettingsGroup.Controls.Add(this.IncludeGST);
            this.SettingsGroup.Controls.Add(this.CompoundingTermMonthsLabel);
            this.SettingsGroup.Controls.Add(this.CompoundingTerm);
            this.SettingsGroup.Controls.Add(this.CompoundingTermLabel);
            this.SettingsGroup.Controls.Add(this.InterestRateLabel);
            this.SettingsGroup.Controls.Add(this.InterestRateAsDecimal);
            this.SettingsGroup.Controls.Add(this.InterestRateAsPercent);
            this.SettingsGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.SettingsGroup.Location = new System.Drawing.Point(0, 0);
            this.SettingsGroup.Name = "SettingsGroup";
            this.SettingsGroup.Size = new System.Drawing.Size(287, 167);
            this.SettingsGroup.TabIndex = 0;
            this.SettingsGroup.TabStop = false;
            this.SettingsGroup.Text = "Settings";
            // 
            // TermLengthInputLabel
            // 
            this.TermLengthInputLabel.AutoSize = true;
            this.TermLengthInputLabel.Location = new System.Drawing.Point(12, 91);
            this.TermLengthInputLabel.Name = "TermLengthInputLabel";
            this.TermLengthInputLabel.Size = new System.Drawing.Size(97, 13);
            this.TermLengthInputLabel.TabIndex = 12;
            this.TermLengthInputLabel.Text = "Term Length Input:";
            this.TermLengthInputLabel.MouseEnter += new System.EventHandler(this.TermLengthSettings_MouseEnter);
            this.TermLengthInputLabel.MouseLeave += new System.EventHandler(this.Setting_MouseLeave);
            // 
            // TermLengthAsYears
            // 
            this.TermLengthAsYears.AutoSize = true;
            this.TermLengthAsYears.Location = new System.Drawing.Point(199, 90);
            this.TermLengthAsYears.Name = "TermLengthAsYears";
            this.TermLengthAsYears.Size = new System.Drawing.Size(65, 17);
            this.TermLengthAsYears.TabIndex = 11;
            this.TermLengthAsYears.Text = "as years";
            this.TermLengthAsYears.UseVisualStyleBackColor = true;
            this.TermLengthAsYears.Click += new System.EventHandler(this.TermLengthAsYears_Click);
            this.TermLengthAsYears.MouseEnter += new System.EventHandler(this.TermLengthSettings_MouseEnter);
            this.TermLengthAsYears.MouseLeave += new System.EventHandler(this.Setting_MouseLeave);
            // 
            // TermLengthAsMonths
            // 
            this.TermLengthAsMonths.AutoSize = true;
            this.TermLengthAsMonths.Checked = true;
            this.TermLengthAsMonths.CheckState = System.Windows.Forms.CheckState.Checked;
            this.TermLengthAsMonths.Location = new System.Drawing.Point(116, 90);
            this.TermLengthAsMonths.Name = "TermLengthAsMonths";
            this.TermLengthAsMonths.Size = new System.Drawing.Size(74, 17);
            this.TermLengthAsMonths.TabIndex = 10;
            this.TermLengthAsMonths.Text = "as months";
            this.TermLengthAsMonths.UseVisualStyleBackColor = true;
            this.TermLengthAsMonths.Click += new System.EventHandler(this.TermLengthAsMonths_Click);
            this.TermLengthAsMonths.MouseEnter += new System.EventHandler(this.TermLengthSettings_MouseEnter);
            this.TermLengthAsMonths.MouseLeave += new System.EventHandler(this.Setting_MouseLeave);
            // 
            // GSTLabel
            // 
            this.GSTLabel.AutoSize = true;
            this.GSTLabel.Location = new System.Drawing.Point(167, 67);
            this.GSTLabel.Name = "GSTLabel";
            this.GSTLabel.Size = new System.Drawing.Size(40, 13);
            this.GSTLabel.TabIndex = 9;
            this.GSTLabel.Text = "GST %";
            this.GSTLabel.MouseEnter += new System.EventHandler(this.SalesTaxSettings_MouseEnter);
            this.GSTLabel.MouseLeave += new System.EventHandler(this.Setting_MouseLeave);
            // 
            // GST
            // 
            this.GST.DecimalPlaces = 3;
            this.GST.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.GST.Location = new System.Drawing.Point(213, 65);
            this.GST.Name = "GST";
            this.GST.Size = new System.Drawing.Size(59, 20);
            this.GST.TabIndex = 8;
            this.GST.Value = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.GST.ValueChanged += new System.EventHandler(this.GST_ValueChanged);
            // 
            // Taxes
            // 
            this.Taxes.AutoSize = true;
            this.Taxes.Location = new System.Drawing.Point(12, 67);
            this.Taxes.Name = "Taxes";
            this.Taxes.Size = new System.Drawing.Size(57, 13);
            this.Taxes.TabIndex = 7;
            this.Taxes.Text = "Sales Tax:";
            this.Taxes.MouseEnter += new System.EventHandler(this.SalesTaxSettings_MouseEnter);
            this.Taxes.MouseLeave += new System.EventHandler(this.Setting_MouseLeave);
            // 
            // IncludeGST
            // 
            this.IncludeGST.AutoSize = true;
            this.IncludeGST.Checked = true;
            this.IncludeGST.CheckState = System.Windows.Forms.CheckState.Checked;
            this.IncludeGST.Location = new System.Drawing.Point(75, 66);
            this.IncludeGST.Name = "IncludeGST";
            this.IncludeGST.Size = new System.Drawing.Size(86, 17);
            this.IncludeGST.TabIndex = 6;
            this.IncludeGST.Text = "Include GST";
            this.IncludeGST.UseVisualStyleBackColor = true;
            this.IncludeGST.CheckedChanged += new System.EventHandler(this.IncludeGST_CheckedChanged);
            this.IncludeGST.MouseEnter += new System.EventHandler(this.SalesTaxSettings_MouseEnter);
            this.IncludeGST.MouseLeave += new System.EventHandler(this.Setting_MouseLeave);
            // 
            // CompoundingTermMonthsLabel
            // 
            this.CompoundingTermMonthsLabel.AutoSize = true;
            this.CompoundingTermMonthsLabel.Location = new System.Drawing.Point(161, 42);
            this.CompoundingTermMonthsLabel.Name = "CompoundingTermMonthsLabel";
            this.CompoundingTermMonthsLabel.Size = new System.Drawing.Size(41, 13);
            this.CompoundingTermMonthsLabel.TabIndex = 5;
            this.CompoundingTermMonthsLabel.Text = "months";
            this.CompoundingTermMonthsLabel.MouseEnter += new System.EventHandler(this.CompoundTermSettings_MouseEnter);
            this.CompoundingTermMonthsLabel.MouseLeave += new System.EventHandler(this.Setting_MouseLeave);
            // 
            // CompoundingTerm
            // 
            this.CompoundingTerm.Location = new System.Drawing.Point(116, 40);
            this.CompoundingTerm.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.CompoundingTerm.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.CompoundingTerm.Name = "CompoundingTerm";
            this.CompoundingTerm.Size = new System.Drawing.Size(39, 20);
            this.CompoundingTerm.TabIndex = 4;
            this.CompoundingTerm.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.CompoundingTerm.ValueChanged += new System.EventHandler(this.CompoundingTerm_ValueChanged);
            // 
            // CompoundingTermLabel
            // 
            this.CompoundingTermLabel.AutoSize = true;
            this.CompoundingTermLabel.Location = new System.Drawing.Point(12, 42);
            this.CompoundingTermLabel.Name = "CompoundingTermLabel";
            this.CompoundingTermLabel.Size = new System.Drawing.Size(102, 13);
            this.CompoundingTermLabel.TabIndex = 3;
            this.CompoundingTermLabel.Text = "Compounding Term:";
            this.CompoundingTermLabel.MouseEnter += new System.EventHandler(this.CompoundTermSettings_MouseEnter);
            this.CompoundingTermLabel.MouseLeave += new System.EventHandler(this.Setting_MouseLeave);
            // 
            // InterestRateLabel
            // 
            this.InterestRateLabel.AutoSize = true;
            this.InterestRateLabel.Location = new System.Drawing.Point(12, 20);
            this.InterestRateLabel.Name = "InterestRateLabel";
            this.InterestRateLabel.Size = new System.Drawing.Size(98, 13);
            this.InterestRateLabel.TabIndex = 2;
            this.InterestRateLabel.Text = "Interest Rate Input:";
            this.InterestRateLabel.MouseEnter += new System.EventHandler(this.InterestRateSettings_MouseEnter);
            this.InterestRateLabel.MouseLeave += new System.EventHandler(this.Setting_MouseLeave);
            // 
            // InterestRateAsDecimal
            // 
            this.InterestRateAsDecimal.AutoSize = true;
            this.InterestRateAsDecimal.Location = new System.Drawing.Point(199, 19);
            this.InterestRateAsDecimal.Name = "InterestRateAsDecimal";
            this.InterestRateAsDecimal.Size = new System.Drawing.Size(76, 17);
            this.InterestRateAsDecimal.TabIndex = 1;
            this.InterestRateAsDecimal.Text = "as decimal";
            this.InterestRateAsDecimal.UseVisualStyleBackColor = true;
            this.InterestRateAsDecimal.Click += new System.EventHandler(this.InterestRateAsDecimal_Click);
            this.InterestRateAsDecimal.MouseEnter += new System.EventHandler(this.InterestRateSettings_MouseEnter);
            this.InterestRateAsDecimal.MouseLeave += new System.EventHandler(this.Setting_MouseLeave);
            // 
            // InterestRateAsPercent
            // 
            this.InterestRateAsPercent.AutoSize = true;
            this.InterestRateAsPercent.Checked = true;
            this.InterestRateAsPercent.CheckState = System.Windows.Forms.CheckState.Checked;
            this.InterestRateAsPercent.Location = new System.Drawing.Point(116, 19);
            this.InterestRateAsPercent.Name = "InterestRateAsPercent";
            this.InterestRateAsPercent.Size = new System.Drawing.Size(76, 17);
            this.InterestRateAsPercent.TabIndex = 0;
            this.InterestRateAsPercent.Text = "as percent";
            this.InterestRateAsPercent.UseVisualStyleBackColor = true;
            this.InterestRateAsPercent.Click += new System.EventHandler(this.InterestRateAsPercent_Click);
            this.InterestRateAsPercent.MouseEnter += new System.EventHandler(this.InterestRateSettings_MouseEnter);
            this.InterestRateAsPercent.MouseLeave += new System.EventHandler(this.Setting_MouseLeave);
            // 
            // InfoGroup
            // 
            this.InfoGroup.Controls.Add(this.SettingInfo);
            this.InfoGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InfoGroup.Location = new System.Drawing.Point(0, 167);
            this.InfoGroup.Name = "InfoGroup";
            this.InfoGroup.Size = new System.Drawing.Size(287, 95);
            this.InfoGroup.TabIndex = 1;
            this.InfoGroup.TabStop = false;
            this.InfoGroup.Text = "Information";
            // 
            // SettingInfo
            // 
            this.SettingInfo.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SettingInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SettingInfo.Location = new System.Drawing.Point(3, 16);
            this.SettingInfo.Name = "SettingInfo";
            this.SettingInfo.ReadOnly = true;
            this.SettingInfo.Size = new System.Drawing.Size(281, 76);
            this.SettingInfo.TabIndex = 0;
            this.SettingInfo.Text = "Hover over a setting to learn more about its use.";
            // 
            // ShowTotal
            // 
            this.ShowTotal.AutoSize = true;
            this.ShowTotal.Location = new System.Drawing.Point(102, 112);
            this.ShowTotal.Name = "ShowTotal";
            this.ShowTotal.Size = new System.Drawing.Size(80, 17);
            this.ShowTotal.TabIndex = 13;
            this.ShowTotal.Text = "Show Total";
            this.ShowTotal.UseVisualStyleBackColor = true;
            this.ShowTotal.CheckedChanged += new System.EventHandler(this.ShowTotal_CheckedChanged);
            this.ShowTotal.MouseEnter += new System.EventHandler(this.ShowTotalSetting_MouseEnter);
            this.ShowTotal.MouseLeave += new System.EventHandler(this.Setting_MouseLeave);
            // 
            // OutputOptionsLabel
            // 
            this.OutputOptionsLabel.AutoSize = true;
            this.OutputOptionsLabel.Location = new System.Drawing.Point(15, 113);
            this.OutputOptionsLabel.Name = "OutputOptionsLabel";
            this.OutputOptionsLabel.Size = new System.Drawing.Size(81, 13);
            this.OutputOptionsLabel.TabIndex = 14;
            this.OutputOptionsLabel.Text = "Output Options:";
            this.OutputOptionsLabel.MouseEnter += new System.EventHandler(this.ShowTotalSetting_MouseEnter);
            this.OutputOptionsLabel.MouseLeave += new System.EventHandler(this.Setting_MouseLeave);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 262);
            this.Controls.Add(this.InfoGroup);
            this.Controls.Add(this.SettingsGroup);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Settings";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Settings_FormClosed);
            this.SettingsGroup.ResumeLayout(false);
            this.SettingsGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GST)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompoundingTerm)).EndInit();
            this.InfoGroup.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox SettingsGroup;
        private System.Windows.Forms.NumericUpDown CompoundingTerm;
        private System.Windows.Forms.Label CompoundingTermLabel;
        private System.Windows.Forms.Label InterestRateLabel;
        private System.Windows.Forms.CheckBox InterestRateAsDecimal;
        private System.Windows.Forms.CheckBox InterestRateAsPercent;
        private System.Windows.Forms.GroupBox InfoGroup;
        private System.Windows.Forms.Label CompoundingTermMonthsLabel;
        private System.Windows.Forms.Label GSTLabel;
        private System.Windows.Forms.NumericUpDown GST;
        private System.Windows.Forms.Label Taxes;
        private System.Windows.Forms.CheckBox IncludeGST;
        private System.Windows.Forms.RichTextBox SettingInfo;
        private System.Windows.Forms.Label TermLengthInputLabel;
        private System.Windows.Forms.CheckBox TermLengthAsYears;
        private System.Windows.Forms.CheckBox TermLengthAsMonths;
        private System.Windows.Forms.CheckBox ShowTotal;
        private System.Windows.Forms.Label OutputOptionsLabel;

    }
}