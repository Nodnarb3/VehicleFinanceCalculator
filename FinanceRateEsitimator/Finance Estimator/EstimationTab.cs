﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Finance_Estimator
{
    public partial class EstimationTab : UserControl
    {
        public EstimationTab()
        {
            InitializeComponent();
            UpdateTabs();
        }

        public double getVehiclePrice()
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(VehiclePrice.Text))
                    return double.Parse(VehiclePrice.Text.Replace("$", ""));
                else
                    return 0d;
            }
            catch (Exception e)
            {
                throw new InvalidInputException("Invalid vehicle price entered!");
            }
        }

        public double getInterestRate()
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(InterestRate.Text))
                    return double.Parse(InterestRate.Text.Replace("%", ""));
                else
                    return 0d;
            }
            catch (Exception e)
            {
                throw new InvalidInputException("Invalid interest rate entered!");
            }
        }

        public int getTerm()
        {
            try
            {
                return int.Parse(TermLength.Text);
            }
            catch (Exception e)
            {
                throw new InvalidInputException("Invalid term entered!");
            }
        }

        public double getPayment()
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(Payment.Text))
                    return double.Parse(Payment.Text.Replace("$", ""));
                else
                    return 0d;
            }
            catch (Exception e)
            {
                throw new InvalidInputException("Invalid payment amount entered!");
            }
        }

        private void CalculateButton_Click(object sender, EventArgs e)
        {
            try
            {
                double v = getVehiclePrice();
                double p = getPayment();
                double r = getInterestRate();
                double gst = Double.Parse(ApplicationSettings.Default.gstAmount.ToString());
                int compoundingPeriod = int.Parse(ApplicationSettings.Default.compundingTerm.ToString());
                int t = getTerm();

                if (ApplicationSettings.Default.addGST) v *= 1 + gst;
                if (ApplicationSettings.Default.interestRateInputFormatIsPercent) r *= 0.01;
                if (!ApplicationSettings.Default.termLengthAsMonths) t *= 12;

                double denominator = (1 - Math.Pow(1 + (r / (double)compoundingPeriod), t * -1));

                if (denominator == 0) denominator = 1;

                double monthly = ((v - p) * (r / (double)compoundingPeriod)) / denominator;
                double total = (monthly * t) + p;

                MonthlyPayment.Text = monthly.ToString("C2");
                Total.Text = total.ToString("C2");
            }
            catch (InvalidInputException exc)
            {
                MessageBox.Show(exc.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void VehiclePrice_Leave(object sender, EventArgs e)
        {
            TextBox textbox = sender as TextBox;
            textbox.Text = getVehiclePrice().ToString("C2");
        }

        private void InterestRate_Leave(object sender, EventArgs e)
        {
            TextBox textbox = sender as TextBox;
            if (ApplicationSettings.Default.interestRateInputFormatIsPercent)
            {
                textbox.Text = getInterestRate() + "%";
            }
            else
            {
                textbox.Text = getInterestRate().ToString("P");
            }
        }

        private void Payment_Leave(object sender, EventArgs e)
        {
            TextBox textbox = sender as TextBox;
            textbox.Text = getPayment().ToString("C2");
        }

        private void TermLength_TextModified(object sender, EventArgs e)
        {
            TextBox textbox = sender as TextBox;
            textbox.Text = textbox.Text.Replace(".", "");
            textbox.Select(textbox.TextLength, 1);
        }

        public void UpdateTabs()
        {
            if (ApplicationSettings.Default.termLengthAsMonths)
                TermLengthMeasurmentLabel.Text = "months";
            else
                TermLengthMeasurmentLabel.Text = "years";

            if (!ApplicationSettings.Default.showTotal)
            {
                Total.Visible = false;
                TotalLabel.Visible = false;
            }
            else
            {
                Total.Visible = true;
                TotalLabel.Visible = true;
            }
        }
    }
}
