﻿namespace Finance_Estimator
{
    partial class EstimationTab
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.InfoGroup = new System.Windows.Forms.GroupBox();
            this.TermLengthMeasurmentLabel = new System.Windows.Forms.Label();
            this.CalculateButton = new System.Windows.Forms.Button();
            this.PaymentLabel = new System.Windows.Forms.Label();
            this.TermLengthLabel = new System.Windows.Forms.Label();
            this.InterestRateLabel = new System.Windows.Forms.Label();
            this.Payment = new System.Windows.Forms.TextBox();
            this.TermLength = new System.Windows.Forms.TextBox();
            this.InterestRate = new System.Windows.Forms.TextBox();
            this.VehiclePrice = new System.Windows.Forms.TextBox();
            this.VehiclePriceLabel = new System.Windows.Forms.Label();
            this.OutputPanel = new System.Windows.Forms.Panel();
            this.TotalLabel = new System.Windows.Forms.Label();
            this.MonthlyLabel = new System.Windows.Forms.Label();
            this.Total = new System.Windows.Forms.TextBox();
            this.MonthlyPayment = new System.Windows.Forms.TextBox();
            this.InfoGroup.SuspendLayout();
            this.OutputPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // InfoGroup
            // 
            this.InfoGroup.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.InfoGroup.Controls.Add(this.TermLengthMeasurmentLabel);
            this.InfoGroup.Controls.Add(this.CalculateButton);
            this.InfoGroup.Controls.Add(this.PaymentLabel);
            this.InfoGroup.Controls.Add(this.TermLengthLabel);
            this.InfoGroup.Controls.Add(this.InterestRateLabel);
            this.InfoGroup.Controls.Add(this.Payment);
            this.InfoGroup.Controls.Add(this.TermLength);
            this.InfoGroup.Controls.Add(this.InterestRate);
            this.InfoGroup.Controls.Add(this.VehiclePrice);
            this.InfoGroup.Controls.Add(this.VehiclePriceLabel);
            this.InfoGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.InfoGroup.Location = new System.Drawing.Point(0, 0);
            this.InfoGroup.Name = "InfoGroup";
            this.InfoGroup.Size = new System.Drawing.Size(274, 151);
            this.InfoGroup.TabIndex = 0;
            this.InfoGroup.TabStop = false;
            this.InfoGroup.Text = "Finance Information";
            // 
            // TermLengthMeasurmentLabel
            // 
            this.TermLengthMeasurmentLabel.AutoSize = true;
            this.TermLengthMeasurmentLabel.Location = new System.Drawing.Point(227, 74);
            this.TermLengthMeasurmentLabel.Name = "TermLengthMeasurmentLabel";
            this.TermLengthMeasurmentLabel.Size = new System.Drawing.Size(41, 13);
            this.TermLengthMeasurmentLabel.TabIndex = 9;
            this.TermLengthMeasurmentLabel.Text = "months";
            // 
            // CalculateButton
            // 
            this.CalculateButton.Location = new System.Drawing.Point(105, 124);
            this.CalculateButton.Name = "CalculateButton";
            this.CalculateButton.Size = new System.Drawing.Size(75, 23);
            this.CalculateButton.TabIndex = 8;
            this.CalculateButton.Text = "Calculate";
            this.CalculateButton.UseVisualStyleBackColor = true;
            this.CalculateButton.Click += new System.EventHandler(this.CalculateButton_Click);
            // 
            // PaymentLabel
            // 
            this.PaymentLabel.AutoSize = true;
            this.PaymentLabel.Location = new System.Drawing.Point(7, 101);
            this.PaymentLabel.Name = "PaymentLabel";
            this.PaymentLabel.Size = new System.Drawing.Size(82, 13);
            this.PaymentLabel.TabIndex = 7;
            this.PaymentLabel.Text = "Down Payment:";
            // 
            // TermLengthLabel
            // 
            this.TermLengthLabel.AutoSize = true;
            this.TermLengthLabel.Location = new System.Drawing.Point(7, 74);
            this.TermLengthLabel.Name = "TermLengthLabel";
            this.TermLengthLabel.Size = new System.Drawing.Size(70, 13);
            this.TermLengthLabel.TabIndex = 6;
            this.TermLengthLabel.Text = "Term Length:";
            // 
            // InterestRateLabel
            // 
            this.InterestRateLabel.AutoSize = true;
            this.InterestRateLabel.Location = new System.Drawing.Point(7, 47);
            this.InterestRateLabel.Name = "InterestRateLabel";
            this.InterestRateLabel.Size = new System.Drawing.Size(71, 13);
            this.InterestRateLabel.TabIndex = 5;
            this.InterestRateLabel.Text = "Interest Rate:";
            // 
            // Payment
            // 
            this.Payment.Location = new System.Drawing.Point(95, 98);
            this.Payment.Name = "Payment";
            this.Payment.Size = new System.Drawing.Size(173, 20);
            this.Payment.TabIndex = 4;
            this.Payment.Text = "$0.00";
            this.Payment.Leave += new System.EventHandler(this.Payment_Leave);
            // 
            // TermLength
            // 
            this.TermLength.Location = new System.Drawing.Point(95, 71);
            this.TermLength.Name = "TermLength";
            this.TermLength.Size = new System.Drawing.Size(130, 20);
            this.TermLength.TabIndex = 3;
            this.TermLength.Text = "0";
            this.TermLength.TextChanged += new System.EventHandler(this.TermLength_TextModified);
            this.TermLength.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TermLength_TextModified);
            // 
            // InterestRate
            // 
            this.InterestRate.Location = new System.Drawing.Point(95, 44);
            this.InterestRate.Name = "InterestRate";
            this.InterestRate.Size = new System.Drawing.Size(173, 20);
            this.InterestRate.TabIndex = 2;
            this.InterestRate.Text = "0.00%";
            this.InterestRate.Leave += new System.EventHandler(this.InterestRate_Leave);
            // 
            // VehiclePrice
            // 
            this.VehiclePrice.Location = new System.Drawing.Point(95, 17);
            this.VehiclePrice.Name = "VehiclePrice";
            this.VehiclePrice.Size = new System.Drawing.Size(173, 20);
            this.VehiclePrice.TabIndex = 1;
            this.VehiclePrice.Text = "$0.00";
            this.VehiclePrice.Leave += new System.EventHandler(this.VehiclePrice_Leave);
            // 
            // VehiclePriceLabel
            // 
            this.VehiclePriceLabel.AutoSize = true;
            this.VehiclePriceLabel.Location = new System.Drawing.Point(7, 20);
            this.VehiclePriceLabel.Name = "VehiclePriceLabel";
            this.VehiclePriceLabel.Size = new System.Drawing.Size(72, 13);
            this.VehiclePriceLabel.TabIndex = 0;
            this.VehiclePriceLabel.Text = "Vehicle Price:";
            // 
            // OutputPanel
            // 
            this.OutputPanel.Controls.Add(this.TotalLabel);
            this.OutputPanel.Controls.Add(this.MonthlyLabel);
            this.OutputPanel.Controls.Add(this.Total);
            this.OutputPanel.Controls.Add(this.MonthlyPayment);
            this.OutputPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.OutputPanel.Location = new System.Drawing.Point(0, 153);
            this.OutputPanel.Name = "OutputPanel";
            this.OutputPanel.Size = new System.Drawing.Size(274, 79);
            this.OutputPanel.TabIndex = 1;
            // 
            // TotalLabel
            // 
            this.TotalLabel.AutoSize = true;
            this.TotalLabel.Location = new System.Drawing.Point(63, 32);
            this.TotalLabel.Name = "TotalLabel";
            this.TotalLabel.Size = new System.Drawing.Size(34, 13);
            this.TotalLabel.TabIndex = 3;
            this.TotalLabel.Text = "Total:";
            // 
            // MonthlyLabel
            // 
            this.MonthlyLabel.AutoSize = true;
            this.MonthlyLabel.Location = new System.Drawing.Point(7, 6);
            this.MonthlyLabel.Name = "MonthlyLabel";
            this.MonthlyLabel.Size = new System.Drawing.Size(91, 13);
            this.MonthlyLabel.TabIndex = 2;
            this.MonthlyLabel.Text = "Monthly Payment:";
            // 
            // Total
            // 
            this.Total.BackColor = System.Drawing.SystemColors.Window;
            this.Total.Location = new System.Drawing.Point(104, 29);
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            this.Total.Size = new System.Drawing.Size(164, 20);
            this.Total.TabIndex = 1;
            this.Total.Text = "$0.00";
            // 
            // MonthlyPayment
            // 
            this.MonthlyPayment.BackColor = System.Drawing.SystemColors.Window;
            this.MonthlyPayment.Location = new System.Drawing.Point(104, 3);
            this.MonthlyPayment.Name = "MonthlyPayment";
            this.MonthlyPayment.ReadOnly = true;
            this.MonthlyPayment.Size = new System.Drawing.Size(164, 20);
            this.MonthlyPayment.TabIndex = 0;
            this.MonthlyPayment.Text = "$0.00";
            // 
            // EstimationTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.OutputPanel);
            this.Controls.Add(this.InfoGroup);
            this.Name = "EstimationTab";
            this.Size = new System.Drawing.Size(274, 232);
            this.InfoGroup.ResumeLayout(false);
            this.InfoGroup.PerformLayout();
            this.OutputPanel.ResumeLayout(false);
            this.OutputPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox InfoGroup;
        private System.Windows.Forms.TextBox VehiclePrice;
        private System.Windows.Forms.Label VehiclePriceLabel;
        private System.Windows.Forms.TextBox Payment;
        private System.Windows.Forms.TextBox TermLength;
        private System.Windows.Forms.TextBox InterestRate;
        private System.Windows.Forms.Label InterestRateLabel;
        private System.Windows.Forms.Label TermLengthLabel;
        private System.Windows.Forms.Label PaymentLabel;
        private System.Windows.Forms.Button CalculateButton;
        private System.Windows.Forms.Panel OutputPanel;
        private System.Windows.Forms.Label TotalLabel;
        private System.Windows.Forms.Label MonthlyLabel;
        private System.Windows.Forms.TextBox Total;
        private System.Windows.Forms.TextBox MonthlyPayment;
        private System.Windows.Forms.Label TermLengthMeasurmentLabel;

    }
}
