﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Finance_Estimator
{
    public partial class SplashScreen : Form
    {
        private static int duration = 1;
        private int timeElapsed = 0;
        private Timer timer = new Timer();

        public SplashScreen()
        {
            InitializeComponent();
            timer.Interval = 1000;
            timer.Tick += timer_Tick;
            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            timeElapsed++;
            if (timeElapsed == duration)
            {
                timer.Stop();
                Close();
            }
        }
    }
}
