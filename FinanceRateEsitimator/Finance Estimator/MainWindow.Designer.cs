﻿namespace Finance_Estimator
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.MainTabPanel = new System.Windows.Forms.TabControl();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolBar = new System.Windows.Forms.ToolStrip();
            this.NewEstimation = new System.Windows.Forms.ToolStripDropDownButton();
            this.NewTab = new System.Windows.Forms.ToolStripMenuItem();
            this.NewTabMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SettingsButton = new System.Windows.Forms.ToolStripMenuItem();
            this.UpdateButton = new System.Windows.Forms.ToolStripMenuItem();
            this.AboutButton = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitButton = new System.Windows.Forms.ToolStripMenuItem();
            this.NewTabButton = new System.Windows.Forms.ToolStripButton();
            this.CloseTab = new System.Windows.Forms.ToolStripButton();
            this.ToolBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainTabPanel
            // 
            this.MainTabPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainTabPanel.Location = new System.Drawing.Point(0, 25);
            this.MainTabPanel.Name = "MainTabPanel";
            this.MainTabPanel.SelectedIndex = 0;
            this.MainTabPanel.Size = new System.Drawing.Size(318, 253);
            this.MainTabPanel.TabIndex = 1;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // ToolBar
            // 
            this.ToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewEstimation,
            this.toolStripSeparator1,
            this.NewTabButton,
            this.CloseTab});
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.Size = new System.Drawing.Size(318, 25);
            this.ToolBar.TabIndex = 0;
            this.ToolBar.Text = "toolStrip1";
            // 
            // NewEstimation
            // 
            this.NewEstimation.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.NewEstimation.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewTab,
            this.SettingsButton,
            this.UpdateButton,
            this.AboutButton,
            this.ExitButton});
            this.NewEstimation.Image = ((System.Drawing.Image)(resources.GetObject("NewEstimation.Image")));
            this.NewEstimation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.NewEstimation.Name = "NewEstimation";
            this.NewEstimation.Size = new System.Drawing.Size(38, 22);
            this.NewEstimation.Text = "File";
            // 
            // NewTab
            // 
            this.NewTab.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewTabMenuItem});
            this.NewTab.Image = global::Finance_Estimator.Properties.Resources.action_add_16xLG;
            this.NewTab.Name = "NewTab";
            this.NewTab.Size = new System.Drawing.Size(171, 22);
            this.NewTab.Text = "New...";
            // 
            // NewTabMenuItem
            // 
            this.NewTabMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("NewTabMenuItem.Image")));
            this.NewTabMenuItem.Name = "NewTabMenuItem";
            this.NewTabMenuItem.Size = new System.Drawing.Size(152, 22);
            this.NewTabMenuItem.Text = "New Tab";
            this.NewTabMenuItem.Click += new System.EventHandler(this.NewTab_Click);
            // 
            // SettingsButton
            // 
            this.SettingsButton.Image = ((System.Drawing.Image)(resources.GetObject("SettingsButton.Image")));
            this.SettingsButton.Name = "SettingsButton";
            this.SettingsButton.Size = new System.Drawing.Size(171, 22);
            this.SettingsButton.Text = "Settings";
            this.SettingsButton.Click += new System.EventHandler(this.SettingsButton_Click);
            // 
            // UpdateButton
            // 
            this.UpdateButton.Image = global::Finance_Estimator.Properties.Resources.refresh_16xLG;
            this.UpdateButton.Name = "UpdateButton";
            this.UpdateButton.Size = new System.Drawing.Size(171, 22);
            this.UpdateButton.Text = "Check for Updates";
            this.UpdateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // AboutButton
            // 
            this.AboutButton.Image = global::Finance_Estimator.Properties.Resources.InfoTooltip_16x;
            this.AboutButton.Name = "AboutButton";
            this.AboutButton.Size = new System.Drawing.Size(171, 22);
            this.AboutButton.Text = "About";
            this.AboutButton.Click += new System.EventHandler(this.AboutButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.Image = global::Finance_Estimator.Properties.Resources.Close_16xLG;
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(171, 22);
            this.ExitButton.Text = "Close";
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // NewTabButton
            // 
            this.NewTabButton.Image = ((System.Drawing.Image)(resources.GetObject("NewTabButton.Image")));
            this.NewTabButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.NewTabButton.Name = "NewTabButton";
            this.NewTabButton.Size = new System.Drawing.Size(74, 22);
            this.NewTabButton.Text = "New Tab";
            this.NewTabButton.Click += new System.EventHandler(this.NewTab_Click);
            // 
            // CloseTab
            // 
            this.CloseTab.Image = ((System.Drawing.Image)(resources.GetObject("CloseTab.Image")));
            this.CloseTab.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CloseTab.Name = "CloseTab";
            this.CloseTab.Size = new System.Drawing.Size(79, 22);
            this.CloseTab.Text = "Close Tab";
            this.CloseTab.Click += new System.EventHandler(this.CloseTab_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 278);
            this.Controls.Add(this.MainTabPanel);
            this.Controls.Add(this.ToolBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainWindow";
            this.Text = "Vehicle Finance Calculator";
            this.ToolBar.ResumeLayout(false);
            this.ToolBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl MainTabPanel;
        private System.Windows.Forms.ToolStripDropDownButton NewEstimation;
        private System.Windows.Forms.ToolStripMenuItem NewTab;
        private System.Windows.Forms.ToolStripMenuItem NewTabMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SettingsButton;
        private System.Windows.Forms.ToolStripMenuItem AboutButton;
        private System.Windows.Forms.ToolStripMenuItem ExitButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton NewTabButton;
        private System.Windows.Forms.ToolStripButton CloseTab;
        private System.Windows.Forms.ToolStrip ToolBar;
        private System.Windows.Forms.ToolStripMenuItem UpdateButton;

    }
}

