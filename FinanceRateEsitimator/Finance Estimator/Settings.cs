﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Finance_Estimator
{
    public partial class Settings : Form
    {
        private TabControl tabset{get; set;}
        public Settings(TabControl tabSet)
        {
            InitializeComponent();
            tabset = tabSet;
            //Load Settings
            InterestRateAsPercent.Checked = ApplicationSettings.Default.interestRateInputFormatIsPercent;
            InterestRateAsDecimal.Checked = !ApplicationSettings.Default.interestRateInputFormatIsPercent;
            IncludeGST.Checked = ApplicationSettings.Default.addGST;
            CompoundingTerm.Value = ApplicationSettings.Default.compundingTerm;
            GST.Value = ApplicationSettings.Default.gstAmount;
            TermLengthAsMonths.Checked = ApplicationSettings.Default.termLengthAsMonths;
            TermLengthAsYears.Checked = !ApplicationSettings.Default.termLengthAsMonths;
            ShowTotal.Checked = ApplicationSettings.Default.showTotal;
        }

        private void InterestRateAsPercent_Click(object sender, EventArgs e)
        {
            if (InterestRateAsDecimal.Checked)
            {
                InterestRateAsDecimal.Checked = false;
                ApplicationSettings.Default.interestRateInputFormatIsPercent = true;
                ApplicationSettings.Default.Save();
            }
            else
            {
                InterestRateAsPercent.Checked = true;
            }
        }

        private void InterestRateAsDecimal_Click(object sender, EventArgs e)
        {
            if (InterestRateAsPercent.Checked)
            {
                InterestRateAsPercent.Checked = false;
                ApplicationSettings.Default.interestRateInputFormatIsPercent = false;
                ApplicationSettings.Default.Save();
            }
            else
            {
                InterestRateAsDecimal.Checked = true;
            }
        }

        private void TermLengthAsMonths_Click(object sender, EventArgs e)
        {
            if (TermLengthAsYears.Checked)
            {
                TermLengthAsYears.Checked = false;
                ApplicationSettings.Default.termLengthAsMonths = true;
                ApplicationSettings.Default.Save();
                updateTabs();
            }
            else
            {
                TermLengthAsMonths.Checked = true;
            }
        }

        private void TermLengthAsYears_Click(object sender, EventArgs e)
        {
            if (TermLengthAsMonths.Checked)
            {
                TermLengthAsMonths.Checked = false;
                ApplicationSettings.Default.termLengthAsMonths = false;
                ApplicationSettings.Default.Save();
                updateTabs();
            }
            else
            {
                TermLengthAsYears.Checked = true;
            }
        }

        private void updateTabs()
        {
            foreach (TabPage t in tabset.TabPages)
            {
                foreach (Control c in t.Controls)
                {
                    if (c is EstimationTab)
                    {
                        EstimationTab et = c as EstimationTab;
                        et.UpdateTabs();
                    }
                }
            }
        }

        private void IncludeGST_CheckedChanged(object sender, EventArgs e)
        {
            if (IncludeGST.Checked)
            {
                GST.Enabled = true;
                ApplicationSettings.Default.addGST = true;
            }
            else
            {
                GST.Enabled = false;
                ApplicationSettings.Default.addGST = false;
            }

            ApplicationSettings.Default.Save();
        }

        private void CompoundingTerm_ValueChanged(object sender, EventArgs e)
        {
            ApplicationSettings.Default.compundingTerm = CompoundingTerm.Value;
            ApplicationSettings.Default.Save();
        }

        private void GST_ValueChanged(object sender, EventArgs e)
        {
            ApplicationSettings.Default.gstAmount = GST.Value;
            ApplicationSettings.Default.Save();
        }

        private void ShowTotal_CheckedChanged(object sender, EventArgs e)
        {
            if (ShowTotal.Checked)
                ApplicationSettings.Default.showTotal = true;
            else
                ApplicationSettings.Default.showTotal = false;
            ApplicationSettings.Default.Save();

            updateTabs();
        }

        private void InterestRateSettings_MouseEnter(object sender, EventArgs e)
        {
            InfoGroup.Text = "Interest Rate Input Format";
            SettingInfo.Text = "- as percent: interest rate is entered as a value out of 100. \n\tie. entering 10 would mean 10% interest." + "\n\n- as decimal: interest rate is entered as a value out of 1. \n\tie. entering 0.10 would mean 10% interest.";
        }

        private void CompoundTermSettings_MouseEnter(object sender, EventArgs e)
        {
            InfoGroup.Text = "Compounding Term";
            SettingInfo.Text = "- how often interest is compounded.";
        }

        private void SalesTaxSettings_MouseEnter(object sender, EventArgs e)
        {
            InfoGroup.Text = "Sales Tax";
            SettingInfo.Text = "- add sales tax to base vehicle price before doing calculations. \n\tie. entering 0.050 would mean 5% sales tax.";
        }

        private void TermLengthSettings_MouseEnter(object sender, EventArgs e)
        {
            InfoGroup.Text = "Term Length Input Format";
            SettingInfo.Text = "- as months: finance term is entered as months. \n\t ie. one year would be entered as 12 months. \n- as years: finance term is entered as a whole number of years. \n\tie. 12 months would be entered as 1 year.";
        }

        private void ShowTotalSetting_MouseEnter(object sender, EventArgs e)
        {
            InfoGroup.Text = "Show Total";
            SettingInfo.Text = "- toggle display of total cost after financing.";
        }

        private void Setting_MouseLeave(object sender, EventArgs e)
        {
            InfoGroup.Text = "Information";
            SettingInfo.Text = "Hover over a setting to learn more about its use.";
        }

        private void Settings_FormClosed(object sender, FormClosedEventArgs e)
        {
            MainWindow.settingsWindowOpen = false;
        }

    }
}
